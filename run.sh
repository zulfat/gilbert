CUDA_VISIBLE_DEVICES=$1 python run_ner.py --crf --use_gazetteers --do_train --do_eval --vocab_file=$BIOBERT_DIR/vocab.txt --bert_config_file=$BIOBERT_DIR/bert_config.json --init_checkpoint=$BIOBERT_DIR/biobert_model.ckpt --num_train_epochs=25.0 --data_dir=$NER_DIR/ --output_dir=$2
python biocodes/detok.py --tokens $2/token_test.txt --labels $2/label_test.txt  --save_to $2/predicted_biobert.txt
#cd /root/DATA/smm4h_2019/ner/fold_1
#paste -d ' ' predicted_biobert.txt test_labels_bio.txt > conll_bert.txt
#cd
#./evaluation/conlleval < /root/DATA/smm4h_2019/ner/fold_1/conll_bert.txt
